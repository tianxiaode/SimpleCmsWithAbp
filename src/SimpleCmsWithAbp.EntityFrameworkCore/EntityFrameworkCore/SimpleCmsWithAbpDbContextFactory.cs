﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using SimpleCmsWithAbp.Configuration;
using SimpleCmsWithAbp.Web;

namespace SimpleCmsWithAbp.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class SimpleCmsWithAbpDbContextFactory : IDesignTimeDbContextFactory<SimpleCmsWithAbpDbContext>
    {
        public SimpleCmsWithAbpDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<SimpleCmsWithAbpDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            SimpleCmsWithAbpDbContextConfigurer.Configure(builder, configuration.GetConnectionString(SimpleCmsWithAbpConsts.ConnectionStringName));

            return new SimpleCmsWithAbpDbContext(builder.Options);
        }
    }
}
