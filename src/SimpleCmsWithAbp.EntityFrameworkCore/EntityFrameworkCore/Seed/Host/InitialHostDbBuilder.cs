﻿namespace SimpleCmsWithAbp.EntityFrameworkCore.Seed.Host
{
    public class InitialHostDbBuilder
    {
        private readonly SimpleCmsWithAbpDbContext _context;

        public InitialHostDbBuilder(SimpleCmsWithAbpDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            new DefaultEditionCreator(_context).Create();
            new DefaultLanguagesCreator(_context).Create();
            new HostRoleAndUserCreator(_context).Create();
            new DefaultSettingsCreator(_context).Create();
            new DefaultApplicationLanguageTextCreator(_context).Create();

            _context.SaveChanges();
        }
    }
}
