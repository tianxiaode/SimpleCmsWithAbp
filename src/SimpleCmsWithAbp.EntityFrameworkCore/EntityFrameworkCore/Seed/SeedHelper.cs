﻿using System;
using System.Transactions;
using Microsoft.EntityFrameworkCore;
using Abp.Dependency;
using Abp.Domain.Uow;
using Abp.EntityFrameworkCore.Uow;
using Abp.MultiTenancy;
using IdentityServer4.EntityFramework.DbContexts;
using SimpleCmsWithAbp.EntityFrameworkCore.Seed.Cms;
using SimpleCmsWithAbp.EntityFrameworkCore.Seed.Host;
using SimpleCmsWithAbp.EntityFrameworkCore.Seed.IdentityServer;
using SimpleCmsWithAbp.EntityFrameworkCore.Seed.Tenants;

namespace SimpleCmsWithAbp.EntityFrameworkCore.Seed
{
    public static class SeedHelper
    {
        public static void SeedHostDb(IIocResolver iocResolver)
        {
            WithDbContext<SimpleCmsWithAbpDbContext>(iocResolver, SeedHostDb);
        }

        public static void SeedHostDb(SimpleCmsWithAbpDbContext context)
        {
            context.SuppressAutoSetTenantId = true;

            // Host seed
            new InitialHostDbBuilder(context).Create();

            // Default tenant seed (in host database).
            new DefaultTenantBuilder(context).Create();
            new TenantRoleAndUserBuilder(context, 1).Create();

            new DefaultCategoryBuilder(context,1).Create();
        }

        private static void WithDbContext<TDbContext>(IIocResolver iocResolver, Action<TDbContext> contextAction)
            where TDbContext : DbContext
        {
            using (var uowManager = iocResolver.ResolveAsDisposable<IUnitOfWorkManager>())
            {
                using (var uow = uowManager.Object.Begin(TransactionScopeOption.Suppress))
                {
                    var context = uowManager.Object.Current.GetDbContext<TDbContext>(MultiTenancySides.Host);

                    contextAction(context);

                    uow.Complete();
                }
            }
        }

        public static void SeedIdentityServerDb(IIocResolver iocResolver)
        {
            WithDbContext<ConfigurationDbContext>(iocResolver, SeedIdentityServerDb);
        }

        public static void SeedIdentityServerDb(ConfigurationDbContext context)
        {
            new InitialIConfigurationDbBuilder(context).Create();
        }

    }
}
