﻿using IdentityServer4.EntityFramework.DbContexts;
using Microsoft.EntityFrameworkCore;

namespace SimpleCmsWithAbp.EntityFrameworkCore.Seed.IdentityServer
{
    public class InitialIConfigurationDbBuilder
    {
        private readonly ConfigurationDbContext _context;

        public InitialIConfigurationDbBuilder(ConfigurationDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            new DefaultApiResourceCreator(_context).Create();
            new DefaultIdentityResourceCreator(_context).Create();
            new DefaultClentCreator(_context).Create();

            _context.SaveChanges();
        }
    }
}
