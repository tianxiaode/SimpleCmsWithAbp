﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using Abp.Configuration;
using Abp.Localization;
using Abp.Net.Mail;
using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Entities;


namespace SimpleCmsWithAbp.EntityFrameworkCore.Seed.IdentityServer
{
    public class DefaultApiResourceCreator
    {
        private readonly ConfigurationDbContext _context;

        public DefaultApiResourceCreator(ConfigurationDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateDefaultApiResource();
        }

        private void CreateDefaultApiResource()
        {
            if (_context.ApiResources.Any()) return;
            var defaultApiResource = new ApiResource() { Name = "default-api", DisplayName = "Default (all) API" };
            _context.ApiResources.Add(defaultApiResource);
            _context.SaveChanges();
        }
    }
}
