﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using SimpleCmsWithAbp.Categories;
using SimpleCmsWithAbp.Editions;
using SimpleCmsWithAbp.MultiTenancy;

namespace SimpleCmsWithAbp.EntityFrameworkCore.Seed.Cms
{
    public class DefaultCategoryBuilder
    {
        private readonly SimpleCmsWithAbpDbContext _context;
        private readonly int _tenantId;


        public DefaultCategoryBuilder(SimpleCmsWithAbpDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            CreateDefaultTenant();
        }

        private void CreateDefaultTenant()
        {
            // Default tenant

            if(_context.Categories.Any(m=>m.Title.Equals("未分类", StringComparison.CurrentCulture))) return;
            var category = new Category() {Title = "未分类", Content = "", SortOrder = 0,TenantId = _tenantId };
            _context.Categories.Add(category);
            _context.SaveChanges();
        }

    }
}
