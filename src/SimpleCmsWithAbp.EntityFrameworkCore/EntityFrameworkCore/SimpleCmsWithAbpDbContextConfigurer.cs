using System;
using System.Data.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SimpleCmsWithAbp.Authorization;

namespace SimpleCmsWithAbp.EntityFrameworkCore
{
    public static class SimpleCmsWithAbpDbContextConfigurer
    {

        public static readonly LoggerFactory MyLoggerFactory
            = new LoggerFactory(new[]
            {
                new Log4NetProvider("log4net.config",new Func<object, Exception, string>((o, exception) =>exception.Message )) 
            });
        public static void Configure(DbContextOptionsBuilder<SimpleCmsWithAbpDbContext> builder, string connectionString)
        {
            //builder.UseSqlServer(connectionString);
            builder.UseLoggerFactory(MyLoggerFactory).UseMySql(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<SimpleCmsWithAbpDbContext> builder, DbConnection connection)
        {
            //builder.UseSqlServer(connection);
            builder.UseLoggerFactory(MyLoggerFactory).UseMySql(connection);
        }
    }
}
