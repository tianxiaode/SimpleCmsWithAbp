﻿using Abp.Domain.Uow;
using Abp.EntityFrameworkCore;
using Abp.MultiTenancy;
using Abp.Zero.EntityFrameworkCore;
using IdentityServer4.EntityFramework.DbContexts;

namespace SimpleCmsWithAbp.EntityFrameworkCore
{
    public class AbpZeroIdentityServerDbMigrator : AbpZeroDbMigrator<ConfigurationDbContext>
    {
        public AbpZeroIdentityServerDbMigrator(
            IUnitOfWorkManager unitOfWorkManager,
            IDbPerTenantConnectionStringResolver connectionStringResolver,
            IDbContextResolver dbContextResolver)
            : base(
                unitOfWorkManager,
                connectionStringResolver,
                dbContextResolver)
        {
        }
    }
}
