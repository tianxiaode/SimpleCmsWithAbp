﻿using Abp.Authorization.Users;
using Abp.Configuration;
using Abp.Extensions;
using Abp.IdentityServer4;
using Abp.Localization;
using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using Castle.Core.Logging;
using SimpleCmsWithAbp.Authorization.Roles;
using SimpleCmsWithAbp.Authorization.Users;
using SimpleCmsWithAbp.Categories;
using SimpleCmsWithAbp.Contents;
using SimpleCmsWithAbp.Mediae;
using SimpleCmsWithAbp.MultiTenancy;
using SimpleCmsWithAbp.Tags;
using SimpleCmsWithAbp.UserProfiles;
using SimpleCmsWithAbp.VerifyCodes;

namespace SimpleCmsWithAbp.EntityFrameworkCore
{
    public class SimpleCmsWithAbpDbContext : AbpZeroDbContext<Tenant, Role, User, SimpleCmsWithAbpDbContext> , IAbpPersistedGrantDbContext
    {
        /* Define an IDbSet for each entity of the application */
        
        public SimpleCmsWithAbpDbContext(DbContextOptions<SimpleCmsWithAbpDbContext> options)
            : base(options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<User>().Property(p => p.EmailAddress).HasMaxLength(255);
            modelBuilder.Entity<User>().Property(p => p.NormalizedEmailAddress).HasMaxLength(255);
            modelBuilder.Entity<Setting>().Property(p => p.Name).HasMaxLength(255);
            modelBuilder.Entity<ApplicationLanguageText>().Property(p => p.Key).HasMaxLength(255);
            modelBuilder.Entity<UserLogin>().Property(p => p.ProviderKey).HasMaxLength(255);


            modelBuilder.ConfigurePersistedGrantEntity();

            modelBuilder.Entity<Category>().HasIndex(p => p.SortOrder);
            modelBuilder.Entity<Content>().HasIndex(p => p.SortOrder);

            modelBuilder.Entity<ContentTag>().HasIndex(p => new {p.ContentId, p.TagId}).IsUnique();
            modelBuilder.Entity<Tag>().HasIndex(p => p.Name).IsUnique();

        }

        public DbSet<PersistedGrantEntity> PersistedGrants { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Content> Contents{ get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<Media> Mediae { get; set; }
        public DbSet<ContentTag> ContentTags { get; set; }
        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<VerifyCode> VerifyCodes { get; set; }

    }

}
