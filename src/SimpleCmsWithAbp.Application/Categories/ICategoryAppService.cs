using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Web.Models;
using SimpleCmsWithAbp.Categories.Dto;

namespace SimpleCmsWithAbp.Categories
{
    public interface ICategoryAppService : IAsyncCrudAppService<CategoryDto,long>
    {
        Task<ListResultDto<GetAllCategoryOutputDto>> GetAll(GetAllCategoryInputDto input);

        Task<PagedResultDto<ComboboxItemDto>> GetSelect(GetSelectCategoryInput input);
    }
    
}
