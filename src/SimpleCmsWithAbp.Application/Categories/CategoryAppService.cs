using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Abp.Localization;
using Abp.Runtime.Session;
using SimpleCmsWithAbp.Authorization;
using SimpleCmsWithAbp.Authorization.Users;
using SimpleCmsWithAbp.Authorization.Roles;
using SimpleCmsWithAbp.Categories;
using SimpleCmsWithAbp.Categories.Dto;
using SimpleCmsWithAbp.Roles.Dto;
using SimpleCmsWithAbp.Users.Dto;
using Microsoft.AspNetCore.Mvc;

namespace SimpleCmsWithAbp.Categories
{
    [AbpAuthorize(PermissionNames.Pages_Articles)]
    public class CategoryAppService : AsyncCrudAppService<Category, CategoryDto, long>, ICategoryAppService
    {

        public CategoryAppService(
            IRepository<Category, long> repository)
            : base(repository)
        {
        }



        [ActionName(nameof(GetAll))]
        public async Task<ListResultDto<GetAllCategoryOutputDto>> GetAll(GetAllCategoryInputDto input)
        {
            CheckGetAllPermission();

            var query = Repository.GetAllIncluding(m => m.Parent).Include(m => m.SubCategories).AsQueryable();

            if (input.Node == -99)
                query = query.Where(m => m.ParentId == null);
            else if (input.Node > 2)
                query = query.Where(m => m.ParentId == input.Node);
            query = query.Where(m => m.Title != "δ����");
            var totalCount = await AsyncQueryableExecuter.CountAsync(query);


            var entities = await AsyncQueryableExecuter.ToListAsync(query.OrderBy(m => m.Title));

            return new ListResultDto<GetAllCategoryOutputDto>(
                entities.Select(MapToGetAllCategoryOutputDto).ToList()
            );
        }



        private GetAllCategoryOutputDto MapToGetAllCategoryOutputDto(Category category )
        {
            var map = ObjectMapper.Map<GetAllCategoryOutputDto>(category);
            map.Leaf = !category.SubCategories.Any();
            return map;
        }

        [NonAction]
        public override Task<PagedResultDto<CategoryDto>> GetAll(PagedAndSortedResultRequestDto input)
        {
            return base.GetAll(input);
        }

        [ActionName(nameof(Create))]
        public async Task<CategoryDto> Create(CreateCategoryDto input)
        {
            CheckCreatePermission();
            var entity = ObjectMapper.Map<Category>(input);
            entity.TenantId = AbpSession.TenantId ?? 1;
            entity.CreatorUserId = AbpSession.UserId;
            await Repository.InsertAsync(entity);
            await CurrentUnitOfWork.SaveChangesAsync();

            return MapToEntityDto(entity);

        }

        [ActionName(nameof(Update))]
        public async Task<CategoryDto> Update(UpdateCategoryDto input)
        {
            CheckUpdatePermission();
            var entity = ObjectMapper.Map<Category>(input);
            entity.LastModifierUserId = AbpSession.UserId;
            await Repository.UpdateAsync(entity);
            await CurrentUnitOfWork.SaveChangesAsync();

            return MapToEntityDto(entity);
        }

        public async Task<PagedResultDto<ComboboxItemDto>> GetSelect(GetSelectCategoryInput input)
        {
            var query = Repository.GetAll();
            if (!string.IsNullOrEmpty(input.Query))
                query = query.Where(m => m.Title.Contains(input.Query));
            query = query.Where(m => m.Title != "δ����");

            var totalCount = await AsyncQueryableExecuter.CountAsync(query);


            var entities = await AsyncQueryableExecuter.ToListAsync(query.OrderBy(m => m.Title));

            return new PagedResultDto<ComboboxItemDto>(
                totalCount,
                entities.Select(m=>new ComboboxItemDto()
                {
                    DisplayText = m.Title,
                    IsSelected = false,
                    Value = m.Id.ToString()
                }).ToList()
            );

        }

        [NonAction]
        public override Task<CategoryDto> Create(CategoryDto input)
        {
            return base.Create(input);
        }

        [NonAction]
        public override Task<CategoryDto> Update(CategoryDto input)
        {
            return base.Update(input);
        }

        public override async Task Delete(EntityDto<long> input)
        {
            CheckDeletePermission();
            var entity = await Repository.GetAllIncluding(m=>m.Contents).FirstOrDefaultAsync(m=>m.Id == input.Id);
            if(entity ==null) return;
            entity.DeleterUserId = AbpSession.UserId;
            foreach (var content in entity.Contents)
            {
                content.CategoryId = 2;
            }
            await base.Delete(input);
        }
    }
}
