﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Application.Services.Dto;

namespace SimpleCmsWithAbp.Categories.Dto
{
    public class GetAllCategoryInputDto
    {
        public int? Node { get; set; }
    }
}
