using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Localization;
using Abp.Runtime.Validation;
using Abp.Timing;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using SimpleCmsWithAbp.Authorization.Users;
using SimpleCmsWithAbp.VerifyCodes;

namespace SimpleCmsWithAbp.Categories.Dto
{
    [AutoMapFrom(typeof(Category))]
    public class CategoryDto : EntityDto<long>
    {
        [Required]
        [StringLength(Category.MaxStringLength)]
        public string Title { get; set; }

        [StringLength(Category.MaxStringLength)]
        public string Image { get; set; }

        [StringLength(Category.MaxContentLength)]
        public string Content { get; set; }

        public int SortOrder { get; set; }

        public DateTime CreationTime { get; set; }

        public long? ParentId { get; set; }

    }
}
