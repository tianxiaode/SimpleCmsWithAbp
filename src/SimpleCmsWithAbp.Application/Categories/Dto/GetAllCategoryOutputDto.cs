﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace SimpleCmsWithAbp.Categories.Dto
{
    [AutoMapFrom(typeof(Category))]
    public class GetAllCategoryOutputDto  : EntityDto<long>
    {

        public string Title { get; set; }

        public long? ParentId { get; set; }

        public int SortOrder { get; set; }

        public string ParentTitle { get; set; }

        public bool Leaf { get; set; }
    }
}
