﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore.Repositories;
using Abp.Localization;
using Abp.Runtime.Validation;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Extensions.DependencyInjection;

namespace SimpleCmsWithAbp.Categories.Dto
{
    [AutoMapTo(typeof(Category))]
    public class CreateCategoryDto : IValidatableObject
    {
        [Required]
        [StringLength(Category.MaxStringLength)]
        public string Title { get; set; }

        [StringLength(Category.MaxStringLength)]
        public string Image { get; set; }

        [StringLength(Category.MaxContentLength)]
        public string Content { get; set; }

        public int SortOrder { get; set; }

        public DateTime CreationTime { get; set; }

        public long? ParentId { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var categoryRepository = validationContext.GetService<IRepository<Category,long>>();
            var localizationManager = validationContext.GetService<ILocalizationManager>();
            if (ParentId == 2 || categoryRepository.Count(m => m.Id == ParentId) == 0)
            {
                yield return new ValidationResult(
                    localizationManager.GetString(SimpleCmsWithAbpConsts.LocalizationSourceName, "parentCategoryInvalid"),
                    new List<string>() { "ParentId" });

            }
        }
    }
}
