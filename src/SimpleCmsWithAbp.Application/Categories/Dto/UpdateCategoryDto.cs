﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Localization;
using Abp.Runtime.Validation;
using Microsoft.Extensions.DependencyInjection;

namespace SimpleCmsWithAbp.Categories.Dto
{
    [AutoMapTo(typeof(Category))]
    public class UpdateCategoryDto : CategoryDto,IValidatableObject
    {
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var categoryRepository = validationContext.GetService<IRepository<Category, long>>();
            var localizationManager = validationContext.GetService<ILocalizationManager>();
            if (ParentId == 2 || categoryRepository.Count(m => m.Id == ParentId) == 0 || categoryRepository.Count(m => m.ParentId == Id && m.Id == ParentId) > 0)
            {
                yield return new ValidationResult(
                    localizationManager.GetString(SimpleCmsWithAbpConsts.LocalizationSourceName, "parentCategoryInvalid"),
                    new List<string>() { "ParentId" });

                }
        }

    }
}