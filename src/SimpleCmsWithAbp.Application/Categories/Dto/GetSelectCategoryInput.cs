﻿using Abp.Application.Services.Dto;

namespace SimpleCmsWithAbp.Categories.Dto
{
    public class GetSelectCategoryInput : PagedResultRequestDto
    {
        public string Query { get; set; }
    }
}