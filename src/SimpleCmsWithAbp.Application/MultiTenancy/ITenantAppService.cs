﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using SimpleCmsWithAbp.MultiTenancy.Dto;

namespace SimpleCmsWithAbp.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}
