﻿using System.Threading.Tasks;
using Abp.Application.Services;
using SimpleCmsWithAbp.Sessions.Dto;

namespace SimpleCmsWithAbp.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
