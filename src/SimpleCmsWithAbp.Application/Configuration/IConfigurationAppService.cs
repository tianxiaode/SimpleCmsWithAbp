﻿using System.Threading.Tasks;
using SimpleCmsWithAbp.Configuration.Dto;

namespace SimpleCmsWithAbp.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
