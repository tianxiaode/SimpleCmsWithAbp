﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using SimpleCmsWithAbp.Configuration.Dto;

namespace SimpleCmsWithAbp.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : SimpleCmsWithAbpAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
