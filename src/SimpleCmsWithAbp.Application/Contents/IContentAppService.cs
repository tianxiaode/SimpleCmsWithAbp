﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using SimpleCmsWithAbp.Categories.Dto;
using SimpleCmsWithAbp.Contents.Dto;

namespace SimpleCmsWithAbp.Contents
{
    public interface IContentAppService : IAsyncCrudAppService<ContentDto, long>
    {
        Task<PagedResultDto<GetAllContentOutputDto>> GetAll(GetAllContentInputDto input);
    }
}