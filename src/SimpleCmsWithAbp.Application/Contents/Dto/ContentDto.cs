﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using SimpleCmsWithAbp.Categories;

namespace SimpleCmsWithAbp.Contents.Dto
{
    [AutoMapFrom(typeof(Content))]
    public class ContentDto :EntityDto<long>
    {
        [Required]
        [MaxLength(Content.MaxStringLength)]
        public string Title { get; set; }

        [Required]
        public long CategoryId { get; set; }

        [MaxLength(Content.MaxStringLength)]
        public string Image { get; set; }

        [MaxLength(Content.MaxSummaryLength)]
        public string Summary { get; set; }

        [Required]
        public string Body { get; set; }

        [Required]
        public int Hits { get; set; }

        [Required]
        public int SortOrder { get; set; }

        public DateTime CreationTime { get; set; }

    }
}