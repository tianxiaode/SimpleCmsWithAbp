﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace SimpleCmsWithAbp.Contents.Dto
{
    [AutoMapTo(typeof(Content))]
    public class UpdateContentDto : CreateContentDto,IEntityDto<long>
    {
        public long Id { get; set; }
    }
}