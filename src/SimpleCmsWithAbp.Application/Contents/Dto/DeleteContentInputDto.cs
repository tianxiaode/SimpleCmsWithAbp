﻿using System.Collections.Generic;

namespace SimpleCmsWithAbp.Contents.Dto
{
    public class DeleteContentInputDto
    {
        public long[] Id { get; set; }
    }
}