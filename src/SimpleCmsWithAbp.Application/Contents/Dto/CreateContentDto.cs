﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Localization;
using Microsoft.Extensions.DependencyInjection;
using SimpleCmsWithAbp.Categories;

namespace SimpleCmsWithAbp.Contents.Dto
{
    [AutoMapTo(typeof(Content))]
    public class CreateContentDto : IValidatableObject
    {
        [Required]
        [MaxLength(Content.MaxStringLength)]
        public string Title { get; set; }

        public long? CategoryId { get; set; }

        [MaxLength(Content.MaxStringLength)]
        public string Image { get; set; }

        [MaxLength(Content.MaxSummaryLength)]
        public string Summary { get; set; }

        [Required]
        public string Body { get; set; }

        [Required]
        public int SortOrder { get; set; }

        public string[] Tags { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var categoryRepository = validationContext.GetService<IRepository<Category, long>>();
            var localizationManager = validationContext.GetService<ILocalizationManager>();
            if (CategoryId == null)
            {
                CategoryId = 2;
            }else if (categoryRepository.Count(m => m.Id == CategoryId) == 0)
            {
                yield return new ValidationResult(
                    localizationManager.GetString(SimpleCmsWithAbpConsts.LocalizationSourceName,
                        "contentCategoryInvalid"),
                    new List<string>() {"CategoryId"});

            }
        }
    }
}