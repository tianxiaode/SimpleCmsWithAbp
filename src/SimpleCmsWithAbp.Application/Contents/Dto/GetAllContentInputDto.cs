﻿using System;
using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using JetBrains.Annotations;
using Newtonsoft.Json.Linq;
using SimpleCmsWithAbp.Helper;

namespace SimpleCmsWithAbp.Contents.Dto
{
    public class GetAllContentInputDto : PagedAndSortedResultRequestDto, IShouldNormalize
    {
        private readonly JObject _allowSorts = new JObject()
        {
            { "id", "Id" },
            { "title", "Title" },
            { "creationTime", "CreationTime" },
            { "sortOrder", "SortOrder" },
            { "hits", "Hits" }
        };

        public long Cid { get; set; }
        public string Query { get; set; }
        public DateTime? StartDateTime { get; set; }
        public DateTime? EndDateTime { get; set; }
        [CanBeNull]
        public string Sort { get; set; }

        public void Normalize()
        {
            if (!string.IsNullOrEmpty(Sort))
            {
                Sorting = Helper.ExtJs.OrderBy(Sort, _allowSorts);
            }
        }
    }
}