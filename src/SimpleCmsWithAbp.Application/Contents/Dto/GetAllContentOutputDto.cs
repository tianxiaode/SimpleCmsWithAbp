﻿using System;
using System.Collections.Generic;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;

namespace SimpleCmsWithAbp.Contents.Dto
{
    [AutoMapFrom(typeof(Content))]
    public class GetAllContentOutputDto : EntityDto<long>
    {
        public string Title { get; set; }

        public long CategoryId { get; set; }

        public string CategoryTitle { get; set; }

        public int Hits { get; set; }

        public int SortOrder { get; set; }

        public DateTime CreationTime { get; set; }

        public string[] Tags { get; set; }

    }
}