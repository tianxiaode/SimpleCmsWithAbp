﻿using System.Threading.Tasks;
using Abp.Application.Services;
using SimpleCmsWithAbp.VerifyCodes.Dto;

namespace SimpleCmsWithAbp.VerifyCodes
{
    public interface IVerifyCodeAppService : IApplicationService
    {
        Task<GetVerifyCodeOutput> Get();
    }
}