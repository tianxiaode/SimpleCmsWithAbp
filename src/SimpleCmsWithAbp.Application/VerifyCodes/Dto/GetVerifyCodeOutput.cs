﻿using System;

namespace SimpleCmsWithAbp.VerifyCodes.Dto
{
    public class GetVerifyCodeOutput
    {
        public string Image { get; set; }
    }
}