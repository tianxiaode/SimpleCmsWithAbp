﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace SimpleCmsWithAbp.Mediae.Dto
{
    public class CreateMediaDto
    {
        [Required]
        public IFormFile File { get; set; }
    }
}