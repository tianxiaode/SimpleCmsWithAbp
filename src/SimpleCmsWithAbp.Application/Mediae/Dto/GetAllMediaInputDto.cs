﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using Newtonsoft.Json.Linq;
using SimpleCmsWithAbp.Helper;

namespace SimpleCmsWithAbp.Mediae.Dto
{
    public class GetAllMediaInputDto : PagedAndSortedResultRequestDto, IShouldNormalize
    {
        private readonly JObject _allowSorts = new JObject()
        {
            { "creationTime", "CreationTime" },
            { "size", "Size" },
            { "description", "Description" }
        };

        public string Query { get; set; }
        public int? Year { get; set; }
        public int? Month { get; set; }
        public int? Day { get; set; }
        [Required]
        public int[] Type { get; set; }
        public string Sort { get; set; }

        public void Normalize()
        {
            if (!string.IsNullOrEmpty(Sort))
            {
                Sorting = Helper.ExtJs.OrderBy(Sort, _allowSorts);
            }
        }
    }
}