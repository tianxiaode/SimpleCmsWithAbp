﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using SimpleCmsWithAbp.Contents.Dto;
using SimpleCmsWithAbp.ExtJs;
using SimpleCmsWithAbp.Mediae.Dto;

namespace SimpleCmsWithAbp.Mediae
{
    public interface IMediaAppService :IAsyncCrudAppService<MediaDto, long>
    {
        
    }
}