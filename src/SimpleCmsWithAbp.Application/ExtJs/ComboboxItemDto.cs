﻿using System;

namespace SimpleCmsWithAbp.ExtJs
{
    [Serializable]
    public class ComboBoxItemDto
    {
        public string Text { get; set; }

        public string Id { get; set; }


        public ComboBoxItemDto()
        {

        }

        public ComboBoxItemDto(string id, string text)
        {
            Text = text;
            Id = id;
        }

    }
}