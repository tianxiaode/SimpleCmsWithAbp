﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using SimpleCmsWithAbp.Tags.Dto;
using SimpleCmsWithAbp.Users.Dto;

namespace SimpleCmsWithAbp.Tags
{
    public interface ITagAppService : IAsyncCrudAppService<TagDto,long>
    {
        Task<PagedResultDto<TagDto>> GetAll(GetAllTagInputDto input);
    }
}