﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace SimpleCmsWithAbp.Tags.Dto
{
    [AutoMapFrom(typeof(Tag))]
    public class TagDto : EntityDto<long>
    {
        [MaxLength(Tag.MaxNameLength)]
        public string Name { get; set; }
    }
}