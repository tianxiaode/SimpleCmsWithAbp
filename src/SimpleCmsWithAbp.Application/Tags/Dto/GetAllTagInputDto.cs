﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using Newtonsoft.Json.Linq;
using SimpleCmsWithAbp.Helper;

namespace SimpleCmsWithAbp.Tags.Dto
{
    public class GetAllTagInputDto : PagedAndSortedResultRequestDto, IShouldNormalize
    {
        private readonly JObject _allowSorts = new JObject()
        {
            { "name", "Name" },
        };

        public long? Cid { get; set; }
        public string Query { get; set; }
        public string Sort { get; set; }
        public void Normalize()
        {
            if (!string.IsNullOrEmpty(Sort))
            {
                Sorting = Helper.ExtJs.OrderBy(Sort, _allowSorts);
            }
        }
    }
}