﻿namespace SimpleCmsWithAbp.Tags.Dto
{
    public class DeleteInputDto
    {
        public string[] Id { get; set; }
    }
}