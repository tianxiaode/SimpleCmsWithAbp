﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Localization;
using Microsoft.Extensions.DependencyInjection;

namespace SimpleCmsWithAbp.Tags.Dto
{
    [AutoMapTo(typeof(Tag))]
    public class CreateTagDto: IValidatableObject
    {
        [MaxLength(Tag.MaxNameLength)]
        public string Name { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var repository = validationContext.GetService<IRepository<Tag, long>>();
            var localizationManager = validationContext.GetService<ILocalizationManager>();
            if (repository.Count(m=>m.Name == Name) > 0)
            {
                yield return new ValidationResult(
                    localizationManager.GetString(SimpleCmsWithAbpConsts.LocalizationSourceName, "tagExists"),
                    new List<string>() { "Name" });

            }
        }
    }
}