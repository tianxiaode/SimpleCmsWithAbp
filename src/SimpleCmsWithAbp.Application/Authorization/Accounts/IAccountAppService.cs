﻿using System.Threading.Tasks;
using Abp.Application.Services;
using SimpleCmsWithAbp.Authorization.Accounts.Dto;

namespace SimpleCmsWithAbp.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
