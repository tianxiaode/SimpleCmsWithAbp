﻿using AutoMapper;
using SimpleCmsWithAbp.Authorization.Users;

namespace SimpleCmsWithAbp.Users.Dto
{
    public class MediaMapProfile : Profile
    {
        public MediaMapProfile()
        {
            CreateMap<UserDto, User>();
            CreateMap<UserDto, User>().ForMember(x => x.Roles, opt => opt.Ignore());

            CreateMap<CreateUserDto, User>();
            CreateMap<CreateUserDto, User>().ForMember(x => x.Roles, opt => opt.Ignore());
        }
    }
}
