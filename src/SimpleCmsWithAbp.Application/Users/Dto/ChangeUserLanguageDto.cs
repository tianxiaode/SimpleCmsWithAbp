using System.ComponentModel.DataAnnotations;

namespace SimpleCmsWithAbp.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}