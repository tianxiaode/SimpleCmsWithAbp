﻿using Abp.Configuration;
using Abp.Localization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using SimpleCmsWithAbp.Configuration;

namespace SimpleCmsWithAbp.Web.Host.Startup
{
    [DependsOn(
       typeof(SimpleCmsWithAbpWebCoreModule))]
    public class SimpleCmsWithAbpWebHostModule: AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public SimpleCmsWithAbpWebHostModule(IHostingEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }


        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(SimpleCmsWithAbpWebHostModule).GetAssembly());
        }
    }
}
