using Microsoft.AspNetCore.Antiforgery;
using SimpleCmsWithAbp.Controllers;

namespace SimpleCmsWithAbp.Web.Host.Controllers
{
    public class AntiForgeryController : SimpleCmsWithAbpControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}
