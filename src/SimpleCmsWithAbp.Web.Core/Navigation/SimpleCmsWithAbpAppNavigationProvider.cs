﻿using Abp.Application.Navigation;
using Abp.Localization;
using SimpleCmsWithAbp.Authorization;

namespace SimpleCmsWithAbp.Navigation
{
    public class SimpleCmsWithAbpAppNavigationProvider : NavigationProvider
    {
        public override void SetNavigation(INavigationProviderContext context)
        {
            context.Manager.MainMenu
                .AddItem(
                    new MenuItemDefinition(
                        "ArticleManagement",
                        L("ArticleManagement"),
                        url: "articleView",
                        icon: "fa fa-file-text-o",
                        requiresAuthentication:true,
                        requiredPermissionName: PermissionNames.Pages_Articles
                    )
                )
                .AddItem(
                    new MenuItemDefinition(
                        "MediaManagement",
                        L("MediaManagement"),
                        url: "mediaView",
                        icon: "fa fa-file-image-o",
                        requiresAuthentication: true,
                        requiredPermissionName: PermissionNames.Pages_Articles
                    )
                )
                .AddItem(
                    new MenuItemDefinition(
                        "UserManagement",
                        L("UserManagement"),
                        url: "userView",
                        icon: "fa fa-user",
                        requiresAuthentication: true,
                        requiredPermissionName: PermissionNames.Pages_Users
                    )
                );
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, SimpleCmsWithAbpConsts.LocalizationSourceName);
        }

    }
}