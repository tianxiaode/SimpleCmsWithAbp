using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace SimpleCmsWithAbp.Controllers
{
    public abstract class SimpleCmsWithAbpControllerBase: AbpController
    {
        protected SimpleCmsWithAbpControllerBase()
        {
            LocalizationSourceName = SimpleCmsWithAbpConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
