﻿using System.Collections.Generic;

namespace SimpleCmsWithAbp.Authentication.External
{
    public interface IExternalAuthConfiguration
    {
        List<ExternalLoginProviderInfo> Providers { get; }
    }
}
