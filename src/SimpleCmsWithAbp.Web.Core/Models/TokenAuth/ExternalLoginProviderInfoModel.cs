﻿using Abp.AutoMapper;
using SimpleCmsWithAbp.Authentication.External;

namespace SimpleCmsWithAbp.Models.TokenAuth
{
    [AutoMapFrom(typeof(ExternalLoginProviderInfo))]
    public class ExternalLoginProviderInfoModel
    {
        public string Name { get; set; }

        public string ClientId { get; set; }
    }
}
