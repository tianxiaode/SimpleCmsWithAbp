﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Auditing;
using Abp.Domain.Entities;

namespace SimpleCmsWithAbp.VerifyCodes
{
    [Table("AppVerifyCodes")]
    public class VerifyCode :Entity<long>
    {
        public const int MaxCodeLength = 6;
        public const int MaxMd5KeyLength = 32;

        [Required]
        [MaxLength(MaxMd5KeyLength)]
        public  string Md5Key { get; set; }


        [Required]
        [MaxLength(MaxCodeLength)]
        public string Code { get; set; }


        [Required]
        public DateTime Expired { get; set; }


    }
}