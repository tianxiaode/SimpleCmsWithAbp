﻿using Abp.MultiTenancy;
using SimpleCmsWithAbp.Authorization.Users;

namespace SimpleCmsWithAbp.MultiTenancy
{
    public class Tenant : AbpTenant<User>
    {
        public Tenant()
        {            
        }

        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {
        }
    }
}
