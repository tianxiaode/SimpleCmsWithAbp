﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Abp.Domain.Entities;
using SimpleCmsWithAbp.Contents;

namespace SimpleCmsWithAbp.Tags
{
    [Table("AppTags")]
    public class Tag: Entity<long>, IMustHaveTenant
    {
        public const int MaxNameLength = 50;

        [Required]
        [MaxLength(MaxNameLength)]
        public string Name { get; set; }

        public int TenantId { get; set; }

        public virtual ICollection<ContentTag> ContentTags { get; set; }

    }
}
