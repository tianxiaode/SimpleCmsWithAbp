﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Abp.Domain.Entities;
using SimpleCmsWithAbp.Authorization.Users;

namespace SimpleCmsWithAbp.UserProfiles
{
    [Table("AppUserProfiles")]
    public class UserProfile :Entity<long>
    {
        public const int MaxKeywordLength = 200;
        public const int MaxValueLength = 1000;

        [DefaultValue(1)]
        public UserProfileType UserProfileType { get; set; }

        public long UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        [Required]
        [MaxLength(MaxKeywordLength)]
        public string Keyword { get; set; }

        [Required]
        [MaxLength(MaxValueLength)]
        public string Value { get; set; }
    }
}
