﻿using Abp.Authorization;
using SimpleCmsWithAbp.Authorization.Roles;
using SimpleCmsWithAbp.Authorization.Users;

namespace SimpleCmsWithAbp.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
