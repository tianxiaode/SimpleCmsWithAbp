﻿using Abp.Configuration.Startup;
using Abp.Localization.Dictionaries;
using Abp.Localization.Dictionaries.Xml;
using Abp.Reflection.Extensions;

namespace SimpleCmsWithAbp.Localization
{
    public static class SimpleCmsWithAbpLocalizationConfigurer
    {
        public static void Configure(ILocalizationConfiguration localizationConfiguration)
        {
            localizationConfiguration.Sources.Add(
                new DictionaryBasedLocalizationSource(SimpleCmsWithAbpConsts.LocalizationSourceName,
                    new XmlEmbeddedFileLocalizationDictionaryProvider(
                        typeof(SimpleCmsWithAbpLocalizationConfigurer).GetAssembly(),
                        "SimpleCmsWithAbp.Localization.SourceFiles"
                    )
                )
            );
        }
    }
}
