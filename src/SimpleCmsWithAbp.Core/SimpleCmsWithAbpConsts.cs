﻿namespace SimpleCmsWithAbp
{
    public class SimpleCmsWithAbpConsts
    {
        public const string LocalizationSourceName = "SimpleCmsWithAbp";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;
    }
}
