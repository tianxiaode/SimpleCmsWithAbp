﻿namespace SimpleCmsWithAbp.Mediae
{
    public enum MediaType: byte
    {
        Image = 0,
        Audio = 1,
        Video = 2

    }
}