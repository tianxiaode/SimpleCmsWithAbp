﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Abp.Timing;

namespace SimpleCmsWithAbp.Mediae
{
    public class Media : Entity<long>, ICreationAudited, IDeletionAudited, IMustHaveTenant
    {
        public const int MaxFileNameLength = 32;
        public const int MaxDescriptionLength = 255;
        public const int MaxPathLength = 10;

        [Required]
        [MaxLength(MaxFileNameLength)]
        public string Filename { get; set; }

        [Required]
        [MaxLength(MaxDescriptionLength)]
        public string Description { get; set; }

        [Required]
        [MaxLength(MaxPathLength)]
        public string Path { get; set; }

        [Required]
        [Range(0, 2)]
        [DefaultValue(0)]
        public MediaType Type { get; set; }

        [Required]
        [DefaultValue(0)]
        public int Size { get; set; }


        public DateTime CreationTime { get; set; }
        public DateTime? DeletionTime { get; set; }
        public long? CreatorUserId { get; set; }
        public long? DeleterUserId { get; set; }
        public bool IsDeleted { get; set; }
        public int TenantId { get; set; }

        public Media()
        {
            CreationTime = Clock.Now;

        }
    }
}
