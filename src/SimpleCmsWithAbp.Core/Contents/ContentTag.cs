﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Abp.Domain.Entities;
using SimpleCmsWithAbp.Tags;

namespace SimpleCmsWithAbp.Contents
{
    [Table("AppContentTags")]
    public class ContentTag:Entity<long>
    {
        [Required]
        public long ContentId { get; set; }
        [ForeignKey("ContentId")]
        public virtual Content Content { get; set; }

        [Required]
        public long TagId { get; set; }
        [ForeignKey("TagId")]
        public virtual Tag Tag { get; set; }

    }
}
